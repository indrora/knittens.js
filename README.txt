knittens.js: stuff.

inspired by the Tumblr fandom-scarves. I made it JQuery-fied.

USAGE:

$.knittens( options );

options:

scarves: array of {"url":"-url of scarf-","title":"flavor text"}
xoffset: Offset in pixels from the left
yoffset: Offset in pixels from the top.
minwidth: Minimum width to display scarves (uses CSS3 selectors)

This takes up the z-index 999; If there's a problem with that, I don't know how to help you.

This injects CSS. These are the things fiddled with:

.scarfbox (main container)
.scarf (container for each scarf)
.scarf[id^="scarf-"] (UID for each scarf).

Override the last and its :hover (make sure to do both!) margin-top if you want to make things look different.

See demo.html for example of usage.