/**
 * knittens.js
 */

;function knittens (options)
    {
     var     defaults = { "scarves":[], "xoffset":0, "yoffset":0,"isCentered":false,"minwidth":0 }; 
      options = $.extend( {}, defaults, options);
      
      // Drop CSS
      
            var TransitionCSS =      " transition: all 0.7s, transform 0.6s; "+
" -moz-transition: all 0.7s ease-in-out; " +
" -webkit-transition: all 0.7s, -webkit-transform 0.5s; " +
" -o-transition: all 0.7s, -o-transform 0.5s;";

    
      var ExtraCSS = ".scarfbox { position:fixed; display:inline-table; ";
      if(options.isCentered)
      {
        ExtraCSS += "left:50%; right:50%; ";
      }
      else
      {
        ExtraCSS += "left:0; top:0;";
      }

      ExtraCSS += "margin-left:"+options.xoffset+"px;";
      
      ExtraCSS += "margin-top: " + options.yoffset + "px; ";
      
      ExtraCSS += "}\n";
      
      ExtraCSS += ".scarf {margin-left:2px; width:25px; display:inline-table; opacity:0.4; margin-top:-100px; " + TransitionCSS + "} \n";
      
      ExtraCSS += "[id^=\"scarf-\"]:hover { opacity: 1.0; margin-top:-65px; "+TransitionCSS+ "}\n";
      
      ExtraCSS += "\n\n\n@media (max-width:"+(options.minwidth)+"px) { .scarfbox{display:none; } }";
      
      ExtraCSS += "\n.scarf a img { border: 0; } /* IE fix */\n";
      
     // Prepend it to the HEAD so that you can have whatever extra styles you want.
     $("head").prepend("<style type=\"text/css\">\n"+ExtraCSS+"\n</style>");

      // Now, drop the scarves.
      
      
      $("body").prepend("<div class=\"scarfbox\" id=\"scarfbox\"></div>")
      options.scarves.forEach(function (scarf,i,a) {
        
        $("#scarfbox").append("<div class=\"scarf\" id=\"scarf-"+i+"\"><a href=\"http://baelor.tumblr.com/scarves\"><img title=\""+scarf.title+"\" src=\""+scarf.url+"\"></a></div>");
        
      });
    
      
    };
    